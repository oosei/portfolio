+++
path = "/"
title = "Current Topics:"
sort_by = "date"
template = "index.html"

[extra]
header = {title = "Hello! I'm Ofosu", img = "img/osei.png", img_alt = "Ofosu Osei, the site owner" }
section_path = "blog/_index.md"
max_posts = 4
social_media_card = "social_cards/index.jpg"
+++

An aspiring data scientist with a clear career goal: to learn and apply mathematics, statistics, specialized programming, advanced analytics, artiﬁcial intelligence, and machine learning with speciﬁc domain expertise that thrives on data science best practices to uncover actionable insights hidden in an organization's data.
